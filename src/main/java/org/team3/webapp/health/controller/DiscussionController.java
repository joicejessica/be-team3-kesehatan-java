package org.team3.webapp.health.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.team3.webapp.health.modules.discussion.service.internal.DiscussionInternal;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest")
public class DiscussionController implements DiscussionInternal {
}
