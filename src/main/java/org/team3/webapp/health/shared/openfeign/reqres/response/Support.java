package org.team3.webapp.health.shared.openfeign.reqres.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Support{

	@JsonProperty("text")
	private String text;

	@JsonProperty("url")
	private String url;
}