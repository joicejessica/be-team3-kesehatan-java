package org.team3.webapp.health.modules.user.service.internal.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.team3.webapp.health.modules.user.dto.request.UserLoginReq;
import org.team3.webapp.health.modules.user.dto.response.UserIsLoginResp;
import org.team3.webapp.health.modules.user.model.entity.UserLogin;
import org.team3.webapp.health.modules.user.service.delegate.UserLoginDelegate;
import org.team3.webapp.health.modules.user.service.internal.UserLoginService;

import java.util.List;
import java.util.Objects;


@Service
@RequiredArgsConstructor
public class UserLoginServiceImpl implements UserLoginService {

    private final UserLoginDelegate userLoginDelegate;

    @Override
    public ResponseEntity<UserIsLoginResp> loginByEmail(UserLoginReq req) {
        List<UserLogin> users = userLoginDelegate.getAllUser();
        UserIsLoginResp resp = new UserIsLoginResp();
        UserLogin isExist = users.stream().filter(userLogin -> Objects.equals(userLogin.getEmail(), req.getEmail()) && Objects.equals(userLogin.getPassword(), req.getPassword()))
                .findFirst().orElse(null);
        resp.setLogin(isExist != null);
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserLogin> register(UserLogin req) {
        var newEntity = userLoginDelegate.insert(req);
        return new ResponseEntity<>(newEntity, HttpStatus.OK);
    }
}
