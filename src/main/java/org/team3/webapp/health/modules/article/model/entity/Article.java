package org.team3.webapp.health.modules.article.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tb_article", schema="db_kesehatan")
@NoArgsConstructor
@AllArgsConstructor
public class Article {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "detail_article")
    private String detailArticle;

    @Column(name = "image")
    private String image;
}
