package org.team3.webapp.health.modules.hospital.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.team3.webapp.health.modules.article.dto.response.ArticleCollectionResp;
import org.team3.webapp.health.modules.hospital.dto.response.HospitalCollectionResp;
import org.team3.webapp.health.modules.hospital.service.internal.HospitalService;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest")
public class HospitalController implements HospitalService{

    private final HospitalService hospitalService;

    @Override
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/hospital-list")
    public ResponseEntity<HospitalCollectionResp> getAllHospital() {
        return hospitalService.getAllHospital();
    }
}
