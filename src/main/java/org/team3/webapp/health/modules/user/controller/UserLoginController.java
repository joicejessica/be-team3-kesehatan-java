package org.team3.webapp.health.modules.user.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.team3.webapp.health.modules.user.dto.request.UserLoginReq;
import org.team3.webapp.health.modules.user.dto.response.UserIsLoginResp;
import org.team3.webapp.health.modules.user.model.entity.UserLogin;
import org.team3.webapp.health.modules.user.service.internal.UserLoginService;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest")
public class UserLoginController implements UserLoginService {

    private final UserLoginService userLoginService;

    @Override
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/login")
    public ResponseEntity<UserIsLoginResp> loginByEmail(@RequestBody UserLoginReq req) {
        return userLoginService.loginByEmail(req);
    }

    @Override
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/register")
    public ResponseEntity<UserLogin> register(@RequestBody UserLogin req) {
        return userLoginService.register(req);
    }

}
