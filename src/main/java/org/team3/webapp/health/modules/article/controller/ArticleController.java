package org.team3.webapp.health.modules.article.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.team3.webapp.health.modules.article.dto.response.ArticleCollectionResp;
import org.team3.webapp.health.modules.article.service.internal.ArticleService;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest")
public class ArticleController implements ArticleService {

    private final ArticleService articleService;

    @Override
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/article-list")
    public ResponseEntity<ArticleCollectionResp> getAllArticle() {
        return articleService.getAllArticle();
    }
}
