package org.team3.webapp.health.modules.article.service.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.team3.webapp.health.modules.article.dto.request.AddArticleReq;
import org.team3.webapp.health.modules.article.model.entity.Article;
import org.team3.webapp.health.modules.article.model.repository.ArticleRepository;
import org.team3.webapp.health.modules.article.service.delegate.ArticleDelegate;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ArticleDelegateImpl implements ArticleDelegate {

    private final ArticleRepository articleRepository;

    @Override
    public List<Article> getAllArticle() {
        return articleRepository.findAll();
    }

    @Override
    public Article saveArticle(Article request) {
        return articleRepository.save(request);
    }
}
