package org.team3.webapp.health.modules.hospital.service.delegate;

import org.team3.webapp.health.modules.hospital.model.entity.Hospital;

import java.util.List;

public interface HospitalDelegate {

    List<Hospital> getAllHospital();
}
