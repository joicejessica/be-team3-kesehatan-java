package org.team3.webapp.health.modules.discussion.service.internal.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.team3.webapp.health.modules.discussion.service.internal.DiscussionInternal;

@Service
@RequiredArgsConstructor
public class DiscussionInternalImpl implements DiscussionInternal {
}
