package org.team3.webapp.health.modules.user.service.internal;

import org.springframework.http.ResponseEntity;
import org.team3.webapp.health.modules.user.dto.request.UserLoginReq;
import org.team3.webapp.health.modules.user.dto.response.UserIsLoginResp;
import org.team3.webapp.health.modules.user.model.entity.UserLogin;

public interface UserLoginService {

    ResponseEntity<UserIsLoginResp> loginByEmail(UserLoginReq req);
    ResponseEntity<UserLogin> register(UserLogin req);

}
