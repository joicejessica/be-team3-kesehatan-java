package org.team3.webapp.health.modules.article.service.internal;

import org.springframework.http.ResponseEntity;
import org.team3.webapp.health.modules.article.dto.response.ArticleCollectionResp;
import org.team3.webapp.health.modules.article.dto.response.ArticleResp;


public interface ArticleService {

    ResponseEntity<ArticleCollectionResp> getAllArticle();

    //ResponseEntity<ArticleResp> saveArticle();
}
