package org.team3.webapp.health.modules.hospital.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;

@NoArgsConstructor
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class HospitalResp {

    private Integer id;
    private String hospitalName;
    private String phone;
    private String email;
    private String address;
    private String city;
}
