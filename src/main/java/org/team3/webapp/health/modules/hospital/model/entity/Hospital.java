package org.team3.webapp.health.modules.hospital.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tb_hospital", schema="db_kesehatan")
@NoArgsConstructor
@AllArgsConstructor
public class Hospital {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(name = "hospital_name")
    private String hospitalName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

}
