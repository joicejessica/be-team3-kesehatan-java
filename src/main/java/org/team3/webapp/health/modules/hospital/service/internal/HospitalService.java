package org.team3.webapp.health.modules.hospital.service.internal;

import org.springframework.http.ResponseEntity;
import org.team3.webapp.health.modules.hospital.dto.response.HospitalCollectionResp;

public interface HospitalService {
    ResponseEntity<HospitalCollectionResp> getAllHospital();
}
