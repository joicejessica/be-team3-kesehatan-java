package org.team3.webapp.health.modules.user.service.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.team3.webapp.health.modules.user.model.entity.UserLogin;
import org.team3.webapp.health.modules.user.model.repository.UserLoginRepository;
import org.team3.webapp.health.modules.user.service.delegate.UserLoginDelegate;

import java.util.List;


@Service
@RequiredArgsConstructor
public class UserLoginDelegateImpl implements UserLoginDelegate {

    private final UserLoginRepository userLoginRepository;

    @Override
    public List<UserLogin> getAllUser() {
        return userLoginRepository.findAll();
    }

    @Override
    public UserLogin getUserById(int id) {
        return userLoginRepository.getById(id);
    }

    @Override
    public UserLogin insert(UserLogin entity) {
        return userLoginRepository.save(entity);
    }
}
