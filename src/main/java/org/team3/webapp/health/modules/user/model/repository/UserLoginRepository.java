package org.team3.webapp.health.modules.user.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.team3.webapp.health.modules.user.model.entity.UserLogin;


@Repository
public interface UserLoginRepository extends JpaRepository<UserLogin, Integer> {

}

