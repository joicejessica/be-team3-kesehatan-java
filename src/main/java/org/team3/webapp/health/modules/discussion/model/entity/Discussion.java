package org.team3.webapp.health.modules.discussion.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tb_article", schema="db_kesehatan")
@NoArgsConstructor
@AllArgsConstructor
public class Discussion {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;
}
