package org.team3.webapp.health.modules.article.service.internal.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.team3.webapp.health.modules.article.dto.response.ArticleCollectionResp;
import org.team3.webapp.health.modules.article.dto.response.ArticleResp;
import org.team3.webapp.health.modules.article.model.entity.Article;
import org.team3.webapp.health.modules.article.service.delegate.ArticleDelegate;
import org.team3.webapp.health.modules.article.service.internal.ArticleService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ArticleServiceImpl implements ArticleService {

    private final ArticleDelegate articleDelegate;

    @Override
    public ResponseEntity<ArticleCollectionResp> getAllArticle() {
        List<Article> articles = articleDelegate.getAllArticle();

        ArticleCollectionResp articleCollectionResp = new ArticleCollectionResp();

        List<ArticleResp> articleRespList = new ArrayList<>();

        for (int i = 0; i < articles.size(); i++){
            ArticleResp articleResp = new ArticleResp();
            articleResp.setId(articles.get(i).getId());
            articleResp.setTitle(articles.get(i).getTitle());
            articleResp.setDetailArticle(articles.get(i).getDetailArticle());
            articleResp.setImage(articles.get(i).getImage());
            articleRespList.add(articleResp);
        }
        articleCollectionResp.setArticles(articleRespList);
        return new ResponseEntity<>(articleCollectionResp, HttpStatus.OK);
    }

    /*@Override
    public ResponseEntity<ArticleResp> saveArticle() {

        return null;
    }*/
}
