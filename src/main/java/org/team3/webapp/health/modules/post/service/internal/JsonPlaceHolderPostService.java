package org.team3.webapp.health.modules.post.service.internal;

import org.team3.webapp.health.modules.post.dto.request.PostReq;
import org.team3.webapp.health.modules.post.dto.response.JsonPlaceHolderPostResponse;
import org.team3.webapp.health.shared.dto.template.ResponseDetail;
import org.team3.webapp.health.shared.dto.template.ResponseList;
import org.team3.webapp.health.shared.dto.template.ResponseTemplate;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface JsonPlaceHolderPostService {

  /**
   * Function used to get All Json Place Holder Post
   * @return ResponseEntity which contains a list of JsonPlaceHolderPost Responses
   */
  ResponseEntity<ResponseTemplate<ResponseList<JsonPlaceHolderPostResponse>>>getAllJsonPlaceHolderPost();

  /**
   * Function used to add Post
   * @param postReq is the body of the request that represents the data to be added
   * @return ResponseEntity which contains a response
   */
  ResponseEntity<ResponseTemplate<ResponseDetail<JsonPlaceHolderPostResponse>>> addPost(PostReq postReq);

  ResponseEntity<ResponseTemplate<ResponseList<JsonPlaceHolderPostResponse>>> getAllJsonPlaceHolderPostPage(
      Pageable pageable);

  ResponseEntity<ResponseTemplate<ResponseDetail<JsonPlaceHolderPostResponse>>> getJsonPlaceHolderPostById(
      Integer id);


}
