package org.team3.webapp.health.modules.hospital.service.internal.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.team3.webapp.health.modules.article.dto.response.ArticleCollectionResp;
import org.team3.webapp.health.modules.article.dto.response.ArticleResp;
import org.team3.webapp.health.modules.article.model.entity.Article;
import org.team3.webapp.health.modules.hospital.dto.response.HospitalCollectionResp;
import org.team3.webapp.health.modules.hospital.dto.response.HospitalResp;
import org.team3.webapp.health.modules.hospital.model.entity.Hospital;
import org.team3.webapp.health.modules.hospital.service.delegate.HospitalDelegate;
import org.team3.webapp.health.modules.hospital.service.internal.HospitalService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HospitalServiceImpl implements HospitalService {

    private final HospitalDelegate hospitalDelegate;

    @Override
    public ResponseEntity<HospitalCollectionResp> getAllHospital() {
        List<Hospital> hospitals = hospitalDelegate.getAllHospital();

        HospitalCollectionResp resp = new HospitalCollectionResp();

        List<HospitalResp> hospitalResps = new ArrayList<>();

        for (int i = 0; i < hospitals.size(); i++){
            HospitalResp hospitalResp = new HospitalResp();
            hospitalResp.setId(hospitals.get(i).getId());
            hospitalResp.setHospitalName(hospitals.get(i).getHospitalName());
            hospitalResp.setPhone(hospitals.get(i).getPhone());
            hospitalResp.setEmail(hospitals.get(i).getEmail());
            hospitalResp.setAddress(hospitals.get(i).getAddress());
            hospitalResp.setCity(hospitals.get(i).getCity());
            hospitalResp.setAddress(hospitals.get(i).getAddress());
            hospitalResps.add(hospitalResp);
        }
        resp.setHospitals(hospitalResps);
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }
}
