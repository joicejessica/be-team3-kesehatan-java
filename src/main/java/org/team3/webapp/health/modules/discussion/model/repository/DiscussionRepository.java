package org.team3.webapp.health.modules.discussion.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.team3.webapp.health.modules.discussion.model.entity.Discussion;

@Repository
public interface DiscussionRepository extends JpaRepository<Discussion, Integer> {
}
