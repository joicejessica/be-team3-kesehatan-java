package org.team3.webapp.health.modules.hospital.service.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.team3.webapp.health.modules.hospital.model.entity.Hospital;
import org.team3.webapp.health.modules.hospital.model.repository.HospitalRepository;
import org.team3.webapp.health.modules.hospital.service.delegate.HospitalDelegate;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HospitalDelegateImpl implements HospitalDelegate {

    private final HospitalRepository hospitalRepository;

    @Override
    public List<Hospital> getAllHospital() {
        return hospitalRepository.findAll();
    }
}
