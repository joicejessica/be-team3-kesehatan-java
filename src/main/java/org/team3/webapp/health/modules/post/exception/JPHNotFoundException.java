package org.team3.webapp.health.modules.post.exception;

import lombok.extern.slf4j.Slf4j;
import org.team3.webapp.health.config.exception.ModuleException;
import org.team3.webapp.health.shared.constant.enums.ResponseEnum;

@Slf4j
public class JPHNotFoundException extends ModuleException {

  public JPHNotFoundException() {
    super(ResponseEnum.JSON_PLACE_HOLDER_POST_NOT_FOUND);
    log.error("JsonPlaceHolder Post not found");
  }
}
