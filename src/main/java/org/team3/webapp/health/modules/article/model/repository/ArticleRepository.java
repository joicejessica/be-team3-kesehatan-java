package org.team3.webapp.health.modules.article.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.team3.webapp.health.modules.article.model.entity.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer> {
}
