package org.team3.webapp.health.modules.user.service.delegate;

import org.team3.webapp.health.modules.user.model.entity.UserLogin;

import java.util.List;

public interface UserLoginDelegate {

    List<UserLogin> getAllUser();

    UserLogin getUserById(int id);
    UserLogin insert(UserLogin entity);
}
