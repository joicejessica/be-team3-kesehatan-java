package org.team3.webapp.health.modules.article.service.delegate;

import org.team3.webapp.health.modules.article.dto.request.AddArticleReq;
import org.team3.webapp.health.modules.article.dto.response.ArticleResp;
import org.team3.webapp.health.modules.article.model.entity.Article;

import java.util.List;

public interface ArticleDelegate {

    List<Article> getAllArticle();

    Article saveArticle(Article request);
}
