package org.team3.webapp.health.modules.discussion.service.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.team3.webapp.health.modules.discussion.service.delegate.DiscussionDelegate;

@Service
@RequiredArgsConstructor
public class DiscussionDelegateImpl implements DiscussionDelegate {
}
